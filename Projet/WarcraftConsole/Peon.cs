using System;

namespace Type
{
    public class Peon
    {
        private string type, race, faction;
        private int hit_point, armor;

        public Peon(string type, string race, string faction, int hit_point, int armor)
        {
            this.type = type;
            this.race = race;
            this.faction = faction;
            this.hit_point = hit_point;
            this.armor = armor;
        }
        public void setType(string type)
        {
            this.type = type;
        }
        public void setRace(string race)
        {
            this.race = race;
        }
        public void setFaction(string faction)
        {
            this.faction = faction;
        }
        public void setHit_point(int hit_point)
        {
            this.hit_point = hit_point;
        }
        public void setArmor(int armor)
        {
            this.armor = armor;
        }
        public string getType()
        {
            return this.type;
        }
        public string getRace()
        {
            return this.race;
        }
        public string getFaction()
        {
            return this.faction;
        }
        public int getHit_point()
        {
            return this.hit_point;
        }
        public int getArmor()
        {
            return this.armor;
        }
        public string sayHello()
        {
            return string.Format("Bonjour je suis un {0} et je fais parti des {1} j'ai rejoins la faction {2}, j'ai {3} points de dégats et {4} points d'armure",this.type, this.race, this.faction, this.hit_point, this.armor);
        }
        public string grunt()
        {
            return "Grrr";
        }
        public string talk()
        {
            return string.Format("Il fait beau aujourd'hui chez les {0}", this.race);
        }
        public string talkToPeon(Peon peon)
        {
            return string.Format("Salut {0}, c'est {1}.", peon.getRace(),this.race);
        }
        public string talkToPeasant(Peasant peasant)
        {
            return string.Format("Retourne dans ton pays sale {0} ou crève !", peasant.getRace());
        }
        
    }
}