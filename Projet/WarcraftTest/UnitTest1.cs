using System;
using Xunit;
using Type;

namespace WarcraftTest
{
    public class UnitTest1
    {
        [Fact]
        public void TestPeonConstructor()
        {
            Peon harold = new Peon("Peon","Orc","Horde",30,0);

            Assert.Equal("Peon", harold.getType());
            Assert.Equal("Orc", harold.getRace());
            Assert.Equal("Horde", harold.getFaction());
            Assert.Equal(30, harold.getHit_point());
            Assert.Equal(0, harold.getArmor());
        }
        [Fact]
        public void TestPeasantConstructor()
        {
            Peasant jacob = new Peasant("Peasant","Human","Alliance",30,0);

            Assert.Equal("Peasant", jacob.getType());
            Assert.Equal("Human", jacob.getRace());
            Assert.Equal("Alliance", jacob.getFaction());
            Assert.Equal(30, jacob.getHit_point());
            Assert.Equal(0, jacob.getArmor());
        }
        [Fact]
        public void TestPeonSayHello()
        {
            Peon harold = new Peon("Peon","Orc","Horde",30,0);

            Assert.Equal("Bonjour je suis un Peon et je fais parti des Orc j'ai rejoins la faction Horde, j'ai 30 points de dégats et 0 points d'armure", harold.sayHello());
        }
        [Fact]
        public void TestPeasantSayHello()
        {
            Peasant jacob = new Peasant("Peasant","Human","Alliance",30,0);

            Assert.Equal("Bonjour je suis un Peasant et je fais parti des Human j'ai rejoins la faction Alliance, j'ai 30 points de dégats et 0 points d'armure", jacob.sayHello());
        }

        [Fact]
        public void TestPeonGrunt()
        {
            Peon harold = new Peon("Peon","Orc","Horde",30,0);

            Assert.Equal("Grrr", harold.grunt());
        }

        [Fact]
        public void TestPeasantGrunt()
        {
            Peasant jacob = new Peasant("Peasant","Human","Alliance",30,0);

            Assert.Equal("Grrr", jacob.grunt());
        }

        [Fact]
        public void TestPeonTalk()
        {
            Peon harold = new Peon("Peon","Orc","Horde",30,0);

            Assert.Equal("Il fait beau aujourd'hui chez les Orc", harold.talk());
        }
        [Fact]
        public void TestPeonTalktoPeon()
        {
            Peon harold = new Peon("Peon","Orc","Horde",30,0);
            Peon jacqueline = new Peon("Peon","Orc","Horde",30,0);

            Assert.Equal("Salut Orc, c'est Orc.", harold.talkToPeon(jacqueline));
        }
        [Fact]
        public void TestPeonTalktoPeasant()
        {
            Peon harold = new Peon("Peon","Orc","Horde",30,0);
            Peasant jacob = new Peasant("Peasant","Human","Alliance",30,0);

            Assert.Equal("Retourne dans ton pays sale Human ou crève !", harold.talkToPeasant(jacob));
        }
        [Fact]
        public void TestPeasantTalk()
        {
            Peasant jacob = new Peasant("Peasant","Human","Alliance",30,0);

            Assert.Equal("Il fait beau aujourd'hui chez les Human", jacob.talk());
        }
        [Fact]
        public void TestPeasantTalkToPeasant()
        {
            Peasant jacob = new Peasant("Peasant","Human","Alliance",30,0);
            Peasant hugette = new Peasant("Peasant","Human","Alliance",30,0);

            Assert.Equal("Salut Human, c'est Human.", jacob.talkToPeasant(hugette));
        }
        [Fact]
        public void TestPeasantTalktoPeon()
        {
            Peon harold = new Peon("Peon","Orc","Horde",30,0);
            Peasant jacob = new Peasant("Peasant","Human","Alliance",30,0);

            Assert.Equal("Retourne dans ton pays sale Orc ou crève !", jacob.talkToPeon(harold));
        }
    }
}
